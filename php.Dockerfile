FROM php:7.3-fpm-alpine
# install mysql pdo driver
RUN docker-php-ext-install pdo_mysql \
    bcmath \
    ctype \
    fileinfo \
    json \
    mbstring \
    tokenizer
#    xml
#    OpenSSL \

# install composer
ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /composer
RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin --filename=composer

# install git
RUN set -eux && \
  apk update && \
  apk add --update --no-cache --virtual=.build-dependencies git

#COPY ./ /var/www/html
#RUN chmod -R 775 /var/www/html/storage
#RUN chmod -R 775 /var/www/html/bootstrap/cache
